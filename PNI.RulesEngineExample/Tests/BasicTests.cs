﻿using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using NUnit.Framework;
using PNI.RulesEngineExample.Core;
using PNI.RulesEngineExample.Infrastructure;
using PNI.RulesEngineExample.Tests;

namespace PNI.RulesEngineExample
{
    [TestFixture]
    public class BasicTests
    {
        private ProductRulesBuilder _rulesBuilder;
        private ProductRulesEngine _rulesEngine;
        

        [SetUp]
        public void BeforeEach()
        {
            _rulesBuilder = new ProductRulesBuilder();
            _rulesEngine = new ProductRulesEngine();
        }

        [Test]
        public void Multiple_rules_can_be_defined()
        {
            var rules = _rulesBuilder.CreateRules(x =>
            {
                x.NewRule(
                    conditions => {
                        conditions.WhenProductTypesAre("Scrim", "Mesh");
                    },
                    actions => {
                        actions.DisallowOption(name: "Finishing", toHaveValue: "Adhesive_Hangers");
                        actions.AllowOtherwise();
                    }
                );
                x.NewRule(
                    conditions => {
                        conditions.WhenProductTypesAre("Scrim2", "Mesh2");
                    },
                    actions => {
                        actions.DisallowOption(name: "Finishing2", toHaveValue: "Adhesive_Hangers2");
                        actions.AllowOtherwise();
                    }
                );
            });

            Assert.That(rules.Count(), Is.AtLeast(2));
        }

        [Test]
        public void Rules_engine_scenario1()
        {
            //given
            var products = LoadProducts();
            var product = products.Single(x => x.Name == "My scrim product 1");
            var rules = _rulesBuilder.CreateRules(
                conditions => {
                    conditions.WhenProductTypesAre("Scrim", "Mesh");
                },
                actions => {
                    actions.DisallowOption(name: "Finishing", toHaveValue: "Adhesive_Hangers");
                    actions.AllowOtherwise();
                }
            );

            //when
            var result = _rulesEngine.CheckIfAllowed(rules, product, optionName: "Finishing", optionValue: "Adhesive_Hangers");

            //then
            Assert.That(result, Is.False);
        }

        [Test]
        public void Rules_engine_scenario2()
        {
            //given
            var products = LoadProducts();
            var product = products.Single(x => x.Name == "test3");
            var rules = _rulesBuilder.CreateRules(
                conditions => {
                    conditions.WhenProductTypeIs("Card");
                    conditions.WhenOptionIsNot(name: "B", value: "2");
                },
                actions => {
                    actions.AllowOption(name: "A", toHaveValue: "1");
                    actions.DisallowOtherwise();
                }
            );

            //when
            var result = _rulesEngine.CheckIfAllowed(rules, product, optionName: "A", optionValue: "1");

            //then
            Assert.That(result, Is.False);
        }

        [Test]
        public void Javascript_engine_scenario()
        {
            //given
            var javascriptEngine = new JavaScriptEngine(); //wraps functionality of https://github.com/JavascriptNet/Javascript.Net framework

            var rulesEngineCode = _rulesEngine.GenerateJavaScriptCode(); //generates JS code using https://github.com/bridgedotnet/Bridge/wiki framework
            javascriptEngine.Run(rulesEngineCode);

            var rules = LoadProductRules();
            var rulesJson = JsonConvert.SerializeObject(rules);
            var product = LoadProducts().Single(x => x.Name == "My scrim product 1");
            var productJson = JsonConvert.SerializeObject(product);
            
            javascriptEngine.SetParameter("optionName", "Finishing");
            javascriptEngine.SetParameter("optionValue", "Adhesive_Hangers");
            javascriptEngine.Run("var rules =" + rulesJson);
            javascriptEngine.Run("var product =" + productJson);
            javascriptEngine.Run("var engine = PNI.RulesEngineExample.RulesEngine();");

            //when
            var result = javascriptEngine.Run(@"
                engine.CheckIfAllowed(rules, product, optionName, optionValue)  // result of expression will be returned from Run mothod of the engine
            ");

            //then
            Assert.That(result, Is.False);
        }

        [Test]
        public void CSharp_DSL_script_scenario()
        {
            //given
            var rulesDSLScript = DSLScripts.DSLScript.ToString();
            var csharpEngine = new CSharpEngine();
            var rules = csharpEngine.ExecuteUntrustedCode<IEnumerable<ProductRule>>(rulesDSLScript);
            var products = LoadProducts();
            var product = products.Single(x => x.Name == "My scrim product 1");

            //when
            var result = _rulesEngine.CheckIfAllowed(rules, product, optionName: "Finishing", optionValue: "Adhesive_Hangers");

            //then
            Assert.That(result, Is.False);
        }

       

        IEnumerable<ProductRule> LoadProductRules()
        {
            return  _rulesBuilder.CreateRules(
               conditions => {
                   conditions.WhenProductTypesAre("Scrim", "Mesh");
               },
               actions => {
                   actions.DisallowOption(name: "Finishing", toHaveValue: "Adhesive_Hangers");
                   actions.AllowOtherwise();
               }
           );
        }

        IEnumerable<Product> LoadProducts()
        {
            var builder = new ProductBuilder();
            var product1 = builder.Create(name: "My scrim product 1", type: "Scrim", options: options =>
            {
                options.Add(name: "Finishing", value: "Adhesive_Hangers");
                options.Add(name: "Color", value: "Blue");
                options.Add(name: "Time To Manufacture", value: "5");
            });
            var product2 = builder.Create(name: "My paper product 2", type: "Paper", options: options =>
            {
                options.Add(name: "Finishing", value: "None");
                options.Add(name: "Color", value: "Green");
                options.Add(name: "Time To Manufacture", value: "2");
            });

            var product3 = builder.Create(name: "test3", type: "Card", options: options =>
            {
                options.Add(name: "B", value: "2");
                options.Add(name: "Color", value: "Green");
                options.Add(name: "Time To Manufacture", value: "2");
            });
            return new[] { product1, product2, product3 };
        }
    }
}