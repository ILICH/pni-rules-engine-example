﻿using System;
using System.Collections.Generic;
using System.Linq;
using PNI.RulesEngineExample.Core;

namespace PNI.RulesEngineExample.Tests
{
    public class DSLScript
    {
        public IEnumerable<ProductRule> Run()
        {
            return new ProductRulesBuilder().CreateRules(
                conditions => {
                    conditions.WhenProductTypesAre("Scrim", "Mesh");
                },
                actions => {
                    actions.DisallowOption(name: "Finishing", toHaveValue: "Adhesive_Hangers");
                    actions.AllowOtherwise();
                }
            );
        }
    }
}