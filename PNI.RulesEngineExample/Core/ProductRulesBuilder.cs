using System;
using System.Collections.Generic;

namespace PNI.RulesEngineExample.Core
{
    public class ProductRulesBuilder
    {
        public IEnumerable<ProductRule> CreateRules(Action<RuleConditionsDsl> conditions, Action<RuleActionsDSL> actions)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<ProductRule> CreateRules(Action<RuleDSL> rules)
        {
            throw new NotImplementedException();
        }
    }

    public class RuleDSL
    {
        public void NewRule(Action<RuleConditionsDsl> conditions, Action<RuleActionsDSL> actions)
        {
            throw new NotImplementedException();
        }
    }

    public class RuleConditionsDsl
    {
        public void WhenProductTypesAre(params string[] typeNames)
        {
            throw new NotImplementedException();
        }

        public void WhenOptionIs(string name, string value)
        {
            throw new NotImplementedException();
        }

        public void WhenProductTypeIs(string typeName)
        {
            throw new NotImplementedException();
        }

        public void WhenOptionIsNot(string name, string value)
        {
            throw new NotImplementedException();
        }
    }
    public class RuleActionsDSL
    {
        public void DisallowOption(string name, string toHaveValue)
        {
            throw new NotImplementedException();
        }

        public void AllowOption(string name, string toHaveValue)
        {
            throw new NotImplementedException();
        }

        public void DisallowOtherwise()
        {
            throw new NotImplementedException();
        }

        public void AllowOtherwise()
        {
            throw new NotImplementedException();
        }
    }
}