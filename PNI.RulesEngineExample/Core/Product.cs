using System.Collections.Generic;

namespace PNI.RulesEngineExample.Core
{
    class Product
    {
        public string Name { get; internal set; }
        public IEnumerable<ProductOption> Options { get; set; }
    }

    class ProductOption
    {
        public bool Selected { get; internal set; }
    }
}