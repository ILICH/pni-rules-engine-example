﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using System.Security;
using System.Security.Permissions;
using System.Security.Policy;

namespace PNI.RulesEngineExample.Infrastructure
{
    //use the following MSDN article to find implementation about how to sandbox code in a separate app domain
    //https://msdn.microsoft.com/en-us/library/bb763046(v=vs.110).aspx
    public class CSharpEngine
    {
        /// <summary>
        /// Runs code in a separate sandbox app domain, with strict security constraints
        /// </summary>
        public T ExecuteUntrustedCode<T>(string dslScript)
        {
            throw new NotImplementedException();
        }
    }
}